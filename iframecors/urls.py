"""iframecors URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import path
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.generic import TemplateView

def api(request):
    if request.user.is_authenticated:
        return JsonResponse({"user": request.user.username})
    return JsonResponse({"user": "not found"})

@xframe_options_exempt
def home(request):
    if not request.user.is_authenticated:
        if User.objects.all().exists():
            user = User.objects.get()
        else:
            user = User.objects.create(username="THE USER")
        login(request, user)
    return render(request, template_name="home.html")

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", home),
    path("include", xframe_options_exempt(TemplateView.as_view(template_name="include.html"))),
    path("api", api),
]
