* edit /etc/hosts
  * `sudo nano /etc/hosts` (on MacOS or Linux)
  * Add `127.0.0.1   iframe.test api.iframe.test somewhere.else` as a separate line
* `./manage.py migrate`
* `./manage.py runserver`
* load each of the pages in a browser in turn:
  * http://api.iframe.test:8000/
  * http://iframe.test:8000/
  * http://somewhere.else:8000/
* On the first two pages, it says it's THE USER at every stage
* On the third page, it does not.